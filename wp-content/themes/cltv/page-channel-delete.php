<?php
/*
Template Name: Delete Channel
*/

use Aws\S3\S3Client;
use Aws\Common\Credentials\Credentials;

$channels = get_posts(array(
  'post_type' => 'channel',
  'posts_per_page' => -1,
  'orderby' => 'title',
  'order' => 'ASC'
));

if(isset($_GET['channel_id'])) {
  cltv_delete_channel($_GET['channel_id']);
}

function cltv_delete_channel($channel_id) {
  $attachment_parents = array($channel_id);

  // get the posts

  $archives = get_posts(array(
    'post_type' => 'archive',
    'post_status' => 'any',
    'posts_per_page' => -1,
    'meta_key' => 'channel',
    'meta_value' => $channel_id
  ));

  $commercials = get_posts(array(
    'post_type' => 'commercial',
    'post_status' => 'any',
    'posts_per_page' => -1,
    'meta_key' => 'channel',
    'meta_value' => $channel_id
  ));

  $sponsors = get_posts(array(
    'post_type' => 'sponsor',
    'post_status' => 'any',
    'posts_per_page' => -1,
    'meta_key' => 'channel',
    'meta_value' => $channel_id
  ));

  // get arrays of post id's

  $archive_id_dict = array_map("cltv_get_post_id", $archives);
  $commercial_id_dict = array_map("cltv_get_post_id", $commercials);
  $sponsor_id_dict = array_map("cltv_get_post_id", $sponsors);

  $attachment_parents = array_merge($attachment_parents, $archive_id_dict, $commercial_id_dict, $sponsor_id_dict);

  $attachments = get_posts(array(
    'post_type' => 'attachment',
    'posts_per_page' => 10,
    'post_parent__in' => $attachment_parents
  ));

  // safe to delete attachment parents
  if(!$attachments) {
    echo 'no attachments!<br><br>';
    unset($attachment_parents[0]);
    cltv_delete_posts($attachment_parents);    
  }
  // delete attachments
  else {
    echo 'attachments<br><br>';
    cltv_delete_attachments($attachments);
  }

}

function cltv_get_post_id($p) {
  return $p->ID;
}

function cltv_delete_posts($post_dict) {
  foreach($post_dict as $post_id) {
    var_dump($post_id); echo '<br><br>';
    wp_delete_post($post_id);    
  }
}

function cltv_delete_attachments($post_dict) {
  foreach($post_dict as $attachment) {    
    $attachment_url = wp_get_attachment_url($attachment->ID);

    var_dump($attachment_url); echo '<br><br>';

    $filename = basename($attachment_url);

    $archives = cltv_delete_from_s3('cltv-archives', $filename, $attachment->ID);
    $recorded = cltv_delete_from_s3('cltv-recordings', $filename, $attachment->ID);

    if(!$archives && !$recorded) {
      wp_delete_post($attachment->ID);
    }
  }
}

function cltv_delete_from_s3($bucket, $filename, $attachment_id) {
  global $environment;
  $credentials = new Credentials($environment['aws']['key'], $environment['aws']['secret']);
  $s3Client = S3Client::factory(array(
      'credentials' => $credentials
  ));

  $iterator = $s3Client->getIterator('ListObjects', array(
    'Bucket' => $bucket,
    'Prefix' => $filename
  ));

  foreach ($iterator as $object) {
    echo 'key: '; var_dump($object['Key']); echo '<br><br>';
    $s3Client->deleteObject(array('Bucket' => $bucket, 'Key' => $object['Key']));
    wp_delete_post($attachment_id);
  }

  return count($iterator);
}

?>

<form method="get">
  <select name="channel_id">
    <?php foreach ($channels as $channel): ?>
      <option value="<?= $channel->ID ?>">
        <?= $channel->post_title ?>
      </option>
    <?php endforeach; ?>
  </select>
  <button type="submit">Submit</button>
</form>