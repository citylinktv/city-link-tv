<?php

  // Define this in wp-config.php
  if (!defined('WP_ENV')) {
    define('WP_ENV', 'production');
  }

  $cltv_aws_account = 'cltv';

  if (of_get_option('cltv_aws_account')) {
    $cltv_aws_account = of_get_option('cltv_aws_account');
  }

  $s3 = array(
    'development' => array(
      'bucket' => 'cltv-archives-dev',
      'dir' => '',
      'guid_prefix' => 'https://d2d26lg03e02ja.cloudfront.net/'
    ),
    'production' => array(
      'bucket' => 'cltv-archives',
      'dir' => '',
      'guid_prefix' => 'https://d33ykqspalhswc.cloudfront.net/'
    )
  );

  $aws = array(
    'key'    => 'AKIAIH3MCUF7CW2LMQ6Q',
    'secret' => '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',
    's3' => $s3[WP_ENV]
  );

  if (of_get_option('cltv_aws_key') && of_get_option('cltv_aws_secret')) {
    $aws['key'] = of_get_option('cltv_aws_key');
    $aws['secret'] = of_get_option('cltv_aws_secret');
  }

  if (of_get_option('cltv_aws_archive_bucket')) {
    $aws['s3']['bucket'] = of_get_option('cltv_aws_archive_bucket');
  }

  if (of_get_option('cltv_aws_archive_guid_prefix')) {
    $aws['s3']['guid_prefix'] = of_get_option('cltv_aws_archive_guid_prefix');
  }

  if (of_get_option('cltv_aws_recording_bucket')) {
    $aws['s3']['recording_bucket'] = of_get_option('cltv_aws_recording_bucket');
  }

  if (of_get_option('cltv_aws_recording_guid_prefix')) {
    $aws['s3']['recording_guid_prefix'] = of_get_option('cltv_aws_recording_guid_prefix');
  }

  if (of_get_option('cltv_upload_bucket_url')) {
    $aws['s3']['upload_bucket_url'] = of_get_option('cltv_upload_bucket_url');
  }

  return array(
    'aws' => $aws
  );

?>
