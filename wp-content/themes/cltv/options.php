<?php
/**
 * A unique identifier is defined to store the options in the database and reference them from the theme.
 * By default it uses the theme name, in lowercase and without spaces, but this can be changed if needed.
 * If the identifier changes, it'll appear as if the options have been reset.
 *
 */

function optionsframework_option_name() {

	// This gets the theme name from the stylesheet (lowercase and without spaces)
	$the_theme = wp_get_theme();
	$themename = $the_theme->Name;
	$themename = preg_replace("/\W/", "", strtolower($themename) );

	$optionsframework_settings = get_option('optionsframework');
	$optionsframework_settings['id'] = $themename;
	update_option('optionsframework', $optionsframework_settings);

	// echo $themename;
}

/**
 * Defines an array of options that will be used to generate the settings page and be saved in the database.
 * When creating the "id" fields, make sure to use all lowercase and no spaces.
 *
 */

function optionsframework_options() {
	$options = array();

	$options[] = array( "name" => "General",
						"type" => "heading");

	$options[] = array( "name" => "Global Message Board",
					"desc" => "",
					"id" => "global_admin_message",
					"std" => "",
					"type" => "text");

	$options[] = array( "name" => "Homepage Image",
					"desc" => "",
					"id" => "cltv_homepage_image",
					"std" => "",
					"type" => "upload");

	$options[] = array( "name" => "Homepage Video",
					"desc" => "",
					"id" => "cltv_homepage_video",
					"std" => "",
					"type" => "text");

	$options[] = array( "name" => "Header Phone Number",
					"desc" => "",
					"id" => "cltv_header_phone",
					"std" => "",
					"type" => "text");

	$options[] = array( "name" => "AWS",
						"type" => "heading");

	$options[] = array( "name" => "Key",
					"desc" => "",
					"id" => "cltv_aws_key",
					"std" => "AKIAIH3MCUF7CW2LMQ6Q",
					"type" => "text");

	$options[] = array( "name" => "Secret",
					"desc" => "",
					"id" => "cltv_aws_secret",
					"std" => "ixWRjMq4FdSQF0x2jR2/2XkkUh8SGIX3aCaDINTb",
					"type" => "text");

	$options[] = array( "name" => "Archive Bucket",
					"desc" => "",
					"id" => "cltv_aws_archive_bucket",
					"std" => "cltv-archives",
					"type" => "text");

	$options[] = array( "name" => "Upload Bucket URL",
					"desc" => "",
					"id" => "cltv_upload_bucket_url",
					"std" => "https://s3-us-west-2.amazonaws.com/mscltv-archives",
					"type" => "text");

	$options[] = array( "name" => "Archive GUID Prefix",
					"desc" => "",
					"id" => "cltv_aws_archive_guid_prefix",
					"std" => "https://d33ykqspalhswc.cloudfront.net/",
					"type" => "text");

	$options[] = array( "name" => "Recording Bucket",
					"desc" => "",
					"id" => "cltv_aws_recording_bucket",
					"std" => "cltv-recordings",
					"type" => "text");

	$options[] = array( "name" => "Recording GUID Prefix",
					"desc" => "",
					"id" => "cltv_aws_recording_guid_prefix",
					"std" => "https://d2suofw2ruzv4m.cloudfront.net/",
					"type" => "text");

	$options[] = array( "name" => "Streaming",
						"type" => "heading");

	$options[] = array( "name" => "Use HTTPS?",
					"desc" => "",
					"id" => "wowza_https",
					"std" => "http",
					"type" => "text");

	$options[] = array( "name" => "Default host",
					"desc" => "",
					"id" => "wowza_default_host",
					"std" => "wowza-sw-1.citylinktv.com",
					"type" => "text");

	$options[] = array( "name" => "Default host",
					"desc" => "",
					"id" => "wowza_default_host",
					"std" => "wowza-sw-1.citylinktv.com",
					"type" => "text");

	$options[] = array( "name" => "us-west-1 host",
					"desc" => "",
					"id" => "wowza_us_west_1_host",
					"std" => "wowza-sw-1.citylinktv.com",
					"type" => "text");

	$options[] = array( "name" => "us-west-2 host",
					"desc" => "",
					"id" => "wowza_us_west_2_host",
					"std" => "wowza-sw-1.citylinktv.com",
					"type" => "text");

	$options[] = array( "name" => "us-west-3 host",
					"desc" => "",
					"id" => "wowza_us_west_3_host",
					"std" => "wowza-sw-1.citylinktv.com",
					"type" => "text");

	$options[] = array( "name" => "talon host",
					"desc" => "",
					"id" => "wowza_talon_host",
					"std" => "wowza-sw-1.citylinktv.com",
					"type" => "text");
	$options[] = array( "name" => "digity host",
					"desc" => "",
					"id" => "wowza_digity_host",
					"std" => "wowza-sw-1.citylinktv.com",
					"type" => "text");
	$options[] = array( "name" => "wireless host",
					"desc" => "",
					"id" => "wowza_wireless_host",
					"std" => "wowza-sw-1.citylinktv.com",
					"type" => "text");
	$options[] = array( "name" => "prepcast host",
					"desc" => "",
					"id" => "wowza_prepcast_host",
					"std" => "wowza-sw-1.citylinktv.com",
					"type" => "text");
	$options[] = array( "name" => "yazoo host",
					"desc" => "",
					"id" => "wowza_yazoo_host",
					"std" => "wowza-sw-1.citylinktv.com",
					"type" => "text");

	$options[] = array( "name" => "tn host",
					"desc" => "",
					"id" => "wowza_tn_host",
					"std" => "wowza-sw-1.citylinktv.com",
					"type" => "text");

	$options[] = array( "name" => "mo host",
					"desc" => "",
					"id" => "wowza_mo_host",
					"std" => "wowza-sw-1.citylinktv.com",
					"type" => "text");

	$options[] = array( "name" => "ks host",
					"desc" => "",
					"id" => "wowza_ks_host",
					"std" => "wowza-sw-1.citylinktv.com",
					"type" => "text");

	$options[] = array( "name" => "ok host",
					"desc" => "",
					"id" => "wowza_ok_host",
					"std" => "wowza-sw-1.citylinktv.com",
					"type" => "text");

	$options[] = array( "name" => "archive host",
					"desc" => "",
					"id" => "wowza_archive_host",
					"std" => "wowza-sw-1.citylinktv.com",
					"type" => "text");

	$options[] = array( "name" => "recorded host",
					"desc" => "",
					"id" => "wowza_recorded_host",
					"std" => "wowza-sw-1.citylinktv.com",
					"type" => "text");

	$options[] = array( "name" => "Woza Server",
					"desc" => "",
					"id" => "wowza_server",
					"std" => "http://wowza.citylinktv.com/",
					"type" => "text");

  $options[] = array( "name" => "Woza CDN",
					"desc" => "",
					"id" => "wowza_cdn",
					"std" => "http://streamcdn.citylinktv.com/",
					"type" => "text");

	$options[] = array( "name" => "Use Woza CDN?",
					"desc" => "",
					"id" => "use_wowza_cdn",
					"type" => "checkbox");
  
    $options[] = array( "name" => "Split Wowza Servers?",
					"desc" => "",
					"id" => "split_wowza_servers",
					"type" => "checkbox");

	$options[] = array( "name" => "Archive RTMP",
						"desc" => "",
						"id" => "archive_rtmp",
						"std" => "rtmp://rtmpuploads.citylinktv.com/cfx/st/",
						"type" => "text");

	$options[] = array( "name" => "Recorded RTMP",
						"desc" => "",
						"id" => "recorded_rtmp",
						"std" => "rtmp://recordings.citylinktv.com/cfx/st/",
						"type" => "text");

	return $options;
}

?>
