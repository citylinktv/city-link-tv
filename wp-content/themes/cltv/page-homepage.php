<?php
/*
Template Name: Homepage
*/

$video = cltv_format_video_src(of_get_option('front_page_video'));
$channels = new WP_Query(array(
	'post_type'=>'channel',
	'orderby'=>'title',
	'order'=>'ASC',
	'posts_per_page'=> 50,
));
$top_channel = get_post(3861);
if(of_get_option('use_wowza_cdn')) {
	$wowza_base = of_get_option('wowza_cdn');
} else {
	$wowza_base = of_get_option('wowza_server');
}
?>

<?php get_header(); ?>
			<div id="content" class="clearfix row-fluid">
				<div class="span3 hidden-phone">
					<div class="tab-content">
						<?php if($channels): ?>
							<div class="row-fluid tab-pane fade in active" id="popular-content">
								<div class="span12">
									<h2>Channels</h2>                                  
                                    
                    <ul class="media-list">
											<?php
											if($channels->have_posts()): while($channels->have_posts()):
												$channels->the_post();
											?>
												<li class="media">
													<a class="pull-left" href="<?php the_permalink(); ?>">
														<?php if(has_post_thumbnail()): ?>
															<?php the_post_thumbnail('thumbnail', array('class'=>'media-object')); ?>
														<?php else: ?>
															<img class="media-object" src="<?php echo get_template_directory_uri(); ?>/images/default_logo.png" alt="" />
														<?php endif; ?>
													</a>
													<div class="media-body">
														<h4 class="media-heading">
															<a href="<?php the_permalink(); ?>" title="<?php the_title(); ?>">
																<?php echo cltv_trim(get_the_title(), 25); ?>
															</a>
														</h4>
													</div>
												</li>
											<?php endwhile; endif; wp_reset_postdata(); ?>
									</ul>

								</div>
							</div>
						<?php endif; ?>
					</div>
				</div>

				<div id="main" class="span9 clearfix" role="main">

					<div class="hero-unit">
						<img class="img-responsive" src="<?= of_get_option('cltv_homepage_image'); ?>" alt="City Link TV">
						<script>
							$(document).ready(function(){
								/*var android = /Android/i.test(navigator.userAgent);
								if(android && navigator.mimeTypes["application/x-shockwave-flash"] == undefined) {
									//$('#video').html($('#android').html());
							    } else {

							    }*/
							  jwplayer.key="FhCglxnrivSEll1OzQL1oW8Q69LdebCHY3JybrAe6ZY";
						    	var theplayer = jwplayer("video").setup({
							        aspectratio: "16:9",
							        width: "100%",
							        skin: "bekle",
							        //title: "Welcome",
							        playlist: [{
							        	image: "https://d33ykqspalhswc.cloudfront.net/poster.jpg",
							        	sources: [{
							        		file: "<?= of_get_option('cltv_homepage_video'); ?>",
                          image: "https://d33ykqspalhswc.cloudfront.net/Live-Streaming-TVC-graphic.jpg"
							        	}]
							        }]
							    });
							});
						</script>
						<div id="video"><a href="<?php echo $wowza_base; ?>vods3/_definst_/mp4:amazons3/cltv-archives/intro2.mp4/playlist.m3u8">Tap here to watch video</a> </div>

						<p>
							<a href="<?php echo get_site_url()."/about" ?>"class="btn btn-primary btn-large">Learn more</a>
							<a href="<?php echo get_site_url()."/register"; ?>" class="btn btn-success btn-large">Get your own channel</a>
						</p>
					</div>

				</div> <!-- end #main -->

			</div> <!-- end #content -->
<?php get_footer(); ?>
