# Setup

This is a standard Wordpress setup. Requirements are PHP, MySQL, and a web server (Apache, NGINX, etc)

1. Import a database dump into MySQL
2. Two columns must be updated with your local/web address:
    * wp_options WHERE option_name = 'siteurl' OR option_name = 'home'

3. Make a copy of wp-config-sample.php and rename it to wp-config.php
4. Edit this file with database connection info
5. The site should run now. Login at /wp-admin
6. Enable the 'cltv' theme

# Development

All functionality is in the cltv theme at ./wp-content/themes/cltv